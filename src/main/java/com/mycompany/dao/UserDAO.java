/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.model.User;
import java.sql.SQLException;
import totalcross.sql.Connection;
import com.mycompany.util.DataBaseManager;
import java.util.ArrayList;
import java.util.List;
import totalcross.db.sqlite.SQLiteConnection;
import totalcross.sql.PreparedStatement;
import totalcross.sql.ResultSet;
import totalcross.sql.Statement;

/**
 *
 * @author guilherme.ribas
 */
public class UserDAO {

    public boolean insertUser(User user) throws SQLException {
        Connection conn = DataBaseManager.getConnection();
        Statement stmt = null;
        StringBuffer sb = new StringBuffer();

        try {
            sb.append("insert into USER(NOME, SOBRENOME, SEXO, IDADE, CPF, ESTADOCIVIL, OBSERVACAO) values(").
                    append("'").
                    append(user.getNome() == null ? "" : user.getNome()).append("','").
                    append(user.getSobrenome() == null ? "" : user.getSobrenome()).append("','").
                    append(user.getSexo() == null ? "" : user.getSexo()).append("','").
                    append(user.getIdade() == null ? "" : user.getIdade()).append("','").
                    append(user.getCpf() == null ? "" : user.getCpf()).append("','").
                    append(user.getEstadoCivil() < 0 ? "" : user.getEstadoCivil()).append("','").
                    append(user.getObservacao() == null ? "" : user.getObservacao()).append("')");
            stmt = conn.createStatement();
            stmt.executeUpdate(sb.toString());
            System.out.println("sucess " + sb);
        } catch (Exception e) {
            System.out.println("StringBuffer " + sb);
            System.out.println("ERRO " + e);
        }
        return true;
    }

    public List<User> getUsers() {
        Statement stmt = null;
        ResultSet rs = null;
        StringBuffer sb = new StringBuffer();
        ArrayList<User> usersLista = new ArrayList<>();
        try {
            Connection conn = DataBaseManager.getConnection();
            stmt = conn.createStatement();
            sb.append("select IDUSER, NOME, SOBRENOME, SEXO, IDADE, CPF, ESTADOCIVIL, OBSERVACAO from user");// where IDUSER = ").append(idUser);
            rs = stmt.executeQuery(sb.toString());

            while (rs.next()) {
                User user = new User();
                user.setIdUser(rs.getInt(1));
                user.setNome(rs.getString(2));
                user.setSobrenome(rs.getString(3));
                user.setSexo(rs.getString(4));
                user.setIdade(rs.getString(5));
                user.setCpf(rs.getString(6));
                user.setEstadoCivil(rs.getInt(7));
                user.setObservacao(rs.getString(8));
                usersLista.add(user);
            }

        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
                stmt.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return usersLista;
    }

    public User getUserByID(int idUser) {
        Statement stmt = null;
        ResultSet rs = null;
        StringBuffer sb = new StringBuffer();
        User userByID = new User();

        try {
            Connection conn = DataBaseManager.getConnection();
            stmt = conn.createStatement();
            sb.append("select IDUSER, NOME, SOBRENOME, SEXO, IDADE, CPF, ESTADOCIVIL, OBSERVACAO from user WHERE IDUSER = ").append(idUser);
            rs = stmt.executeQuery(sb.toString());

            while (rs.next()) {
                userByID.setIdUser(rs.getInt(1));
                userByID.setNome(rs.getString(2));
                userByID.setSobrenome(rs.getString(3));
                userByID.setSexo(rs.getString(4));
                userByID.setIdade(rs.getString(5));
                userByID.setCpf(rs.getString(6));
                userByID.setEstadoCivil(rs.getInt(7));
                userByID.setObservacao(rs.getString(8));
            }

        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
                stmt.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return userByID;
    }

    public int usersListSize() {
        List<User> teste = getUsers();
        int tamanho = teste.size();
        return tamanho;
    }

    public boolean updateUser(User user) throws SQLException {
        Statement stmt = null;
        StringBuffer sb = new StringBuffer();

        try {
            Connection conn = DataBaseManager.getConnection();
            sb.append("update USER SET NOME = '").
                    append(user.getNome() == null ? "" : user.getNome()).append("', ").
                    append("SOBRENOME = '").append(user.getSobrenome() == null ? "" : user.getSobrenome()).append("', ").
                    append("SEXO = '").append(user.getSexo() == null ? "" : user.getSexo()).append("', ").
                    append("IDADE = '").append(user.getIdade() == null ? "" : user.getIdade()).append("', ").
                    append("CPF = '").append(user.getCpf() == null ? "" : user.getCpf()).append("', ").
                    append("ESTADOCIVIL = ").append(user.getEstadoCivil() < 0 ? "" : user.getEstadoCivil()).append(", ").
                    append("OBSERVACAO = '").append(user.getObservacao() == null ? "" : user.getObservacao()).append("' ").
                    append("WHERE IDUSER = ").
                    append(user.getIdUser());
            stmt = conn.createStatement();
            stmt.executeUpdate(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public boolean deleteUser(int idUser) throws SQLException {
        Statement stmt = null;
        StringBuffer sb = new StringBuffer();

        try {
            Connection conn = DataBaseManager.getConnection();
            sb.append("delete from USER where IDUSER = ").append(idUser);
            stmt = conn.createStatement();
            stmt.executeUpdate(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("STRING BUFFER " + sb);
            try {
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

}
