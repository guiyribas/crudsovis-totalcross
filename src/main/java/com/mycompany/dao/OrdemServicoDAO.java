/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.model.OrdemServico;
import com.mycompany.util.DataBaseManager;
import java.sql.SQLException;
import totalcross.sql.Connection;
import totalcross.sql.Statement;

/**
 *
 * @author guilherme.ribas
 */
public class OrdemServicoDAO {

    public boolean insertOrdemServico(OrdemServico os) throws SQLException {
        Connection conn = DataBaseManager.getConnection();
        Statement stmt = null;
        StringBuffer sb = new StringBuffer();

        try {
            sb.append("insert into ordem_servico(TIPO, LOCALIDADE, BAIRRO, CIDADE, IDUSER) values(").
                    append("'").
                    append(os.getTipoOS() == null ? "" : os.getTipoOS()).append("','").
                    append(os.getLocalidadeOS() == null ? "" : os.getLocalidadeOS()).append("','").
                    append(os.getBairroOS() == null ? "" : os.getBairroOS()).append("','").
                    append(os.getCidadeOS() == null ? "" : os.getCidadeOS()).append("','").
                    append(os.getIdUser() < 0 ? "" : os.getIdUser()).append("')");
            stmt = conn.createStatement();
            stmt.executeUpdate(sb.toString());
            System.out.println("sucess " + sb);
        } catch (Exception e) {
            System.out.println("StringBuffer " + sb);
            System.out.println("ERRO " + e);
        }
        return true;
    }

}
