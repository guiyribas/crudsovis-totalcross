/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.util;

import totalcross.ui.Window;
import totalcross.ui.dialog.MessageBox;
import totalcross.ui.gfx.Color;

/**
 *
 * @author guilherme.ribas
 */
public class MessageGeneric {

    public static void errorMessageBox(String title, String msg) {
        MessageBox box = new MessageBox(title, msg);
        box.setBackColor(0xd53032);
        box.popup();
    }

    public static void successMessageBox(String title, String msg) {
        MessageBox mb = new MessageBox(title, msg);
        mb.setBackForeColors(Color.WHITE, Color.BLACK);
        mb.popup();
    }

    public static void alertMessageBox(String title, String msg) {
        MessageBox box = new MessageBox(title, msg);
        box.setBackColor(Color.YELLOW);
        box.popup();
    }
}
