/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.util;

import java.sql.SQLException;
import totalcross.db.sqlite.SQLiteUtil;
import totalcross.sql.Connection;
import totalcross.sql.Statement;
import totalcross.sys.Settings;

/**
 *
 * @author guilherme.ribas
 */
public class DataBaseManager {

    public static SQLiteUtil sqliteUtil;

    static {
        try {
            sqliteUtil = new SQLiteUtil(Settings.appPath, "crudsovis.db");
            Statement st = sqliteUtil.con().createStatement();
            st.execute("create table if not exists ordem_servico "
                    + "(IDOS integer primary key autoincrement, "
                    + "TIPO varchar, LOCALIDADE varchar, BAIRRO varchar, CIDADE varchar, IDUSER integer)");
            st.execute("create table if not exists user "
                    + "(IDUSER integer primary key autoincrement, "
                    + "NOME varchar, SOBRENOME varchar, SEXO varchar, IDADE varchar , CPF varchar, ESTADOCIVIL varchar, OBSERVACAO varchar)");
            st.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {
        return sqliteUtil.con();
    }
}
