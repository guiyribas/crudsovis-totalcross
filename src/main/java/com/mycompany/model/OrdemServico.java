/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

/**
 *
 * @author guilherme.ribas
 */
public class OrdemServico {

    private int idOS;
    private String tipoOS;
    private String localidadeOS;
    private String bairroOS;
    private String cidadeOS;
    private int idUser;

    public OrdemServico() {

    }

    public OrdemServico(int idOS, String tipoOS, String localidadeOS, String bairroOS, String cidadeOS, int idUser) {
        this.idOS = idOS;
        this.tipoOS = tipoOS;
        this.localidadeOS = localidadeOS;
        this.bairroOS = bairroOS;
        this.cidadeOS = cidadeOS;
        this.idUser = idUser;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdOS() {
        return idOS;
    }

    public void setIdOS(int idOS) {
        this.idOS = idOS;
    }

    public String getTipoOS() {
        return tipoOS;
    }

    public void setTipoOS(String tipoOS) {
        this.tipoOS = tipoOS;
    }

    public String getLocalidadeOS() {
        return localidadeOS;
    }

    public void setLocalidadeOS(String localidadeOS) {
        this.localidadeOS = localidadeOS;
    }

    public String getBairroOS() {
        return bairroOS;
    }

    public void setBairroOS(String bairroOS) {
        this.bairroOS = bairroOS;
    }

    public String getCidadeOS() {
        return cidadeOS;
    }

    public void setCidadeOS(String cidadeOS) {
        this.cidadeOS = cidadeOS;
    }

}
