package com.mycompany.control;

import com.mycompany.dao.UserDAO;
import com.mycompany.model.User;
import com.mycompany.ui.usuarios.UserForm;
import com.mycompany.util.CpfValidator;
import com.mycompany.util.MessageGeneric;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import totalcross.ui.Edit;
import totalcross.ui.dialog.MessageBox;
import totalcross.ui.gfx.Color;

/**
 *
 * @author guilherme.ribas
 */
public class UserFormControl {

    private static boolean success = false;

    public UserFormControl() {
    }

    public static boolean ValidateUserForm(String[] labels, User userInput, boolean isEdit) {

        for (int i = 0; i < labels.length; i++) {
            // Valida Nome
            if ("Nome".equals(labels[i])) {
                if (userInput.getNome().equals("")) {
                    MessageGeneric.errorMessageBox("Erro ao criar usuário", "Nome obrigatório");
                    return false;
                }
            }

            // Valida Sobrenome
            if ("Sobrenome".equals(labels[i])) {
                if (userInput.getSobrenome().equals("")) {
                    MessageGeneric.errorMessageBox("Erro ao criar usuário", "Sobrenomeome obrigatório");
                    return false;
                }
            }

            // Valida Sexo (se deixar um radio como padrao nao precisa fazer a validação (???) )
            if ("Sexo".equals(labels[i])) {
                if (userInput.getSexo().equals("")) {
                    MessageGeneric.errorMessageBox("Erro ao criar usuário", "Sexo obrigatório");
                    return false;
                }
            }

            // Valido CPF
            if ("CPF".equals(labels[i])) {
                boolean isCpfValid = CpfValidator.isCPF(userInput.getCpf());
                System.out.println("retorna isCpfValid " + isCpfValid);
                if (isCpfValid == false) {
                    MessageGeneric.errorMessageBox("Erro ao criar usuário", "CPF Inválido");
                    return false;
                }
            }

            // Valida Estado Civil
            if ("Estado Civil".equals(labels[i])) {
                if (userInput.getEstadoCivil() < 0) {
                    MessageGeneric.errorMessageBox("Erro ao criar usuário", "Estado civil obrigatório");
                    return false;
                }
            }
        }
        try {
            if (isEdit) {
                success = new UserDAO().updateUser(userInput);
            } else {
                success = new UserDAO().insertUser(userInput);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return true;
    }
}
