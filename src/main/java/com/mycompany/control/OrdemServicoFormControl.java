/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.control;

import com.mycompany.dao.OrdemServicoDAO;
import com.mycompany.model.OrdemServico;
import java.sql.SQLException;

/**
 *
 * @author guilherme.ribas
 */
public class OrdemServicoFormControl {
    
    private static boolean success = false;
    
    public OrdemServicoFormControl() {
        
    }
    
    public static boolean ValidateOrdemServicoForm(OrdemServico os) throws SQLException{
        // only create
        success = new OrdemServicoDAO().insertOrdemServico(os);
        return true;
    }
    
}
