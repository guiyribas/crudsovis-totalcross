/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ui.os;

import com.mycompany.control.OrdemServicoFormControl;
import com.mycompany.dao.UserDAO;
import com.mycompany.model.OrdemServico;
import com.mycompany.model.User;
import com.mycompany.ui.Default;
import com.mycompany.ui.usuarios.FooterUserForm;
import com.mycompany.ui.usuarios.UserForm;
import com.mycompany.util.Colors;
import com.mycompany.util.MessageGeneric;
import java.util.List;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import totalcross.ui.Button;
import totalcross.ui.ComboBox;
import totalcross.ui.Container;
import static totalcross.ui.Control.AFTER;
import static totalcross.ui.Control.BOTTOM;
import static totalcross.ui.Control.CENTER;
import static totalcross.ui.Control.FILL;
import static totalcross.ui.Control.LEFT;
import static totalcross.ui.Control.PREFERRED;
import static totalcross.ui.Control.TOP;
import totalcross.ui.Edit;
import totalcross.ui.Label;
import totalcross.ui.ScrollContainer;
import totalcross.ui.gfx.Color;
import totalcross.util.ElementNotFoundException;

/**
 *
 * @author guilherme.ribas
 */
public class OrdemServicoForm extends Default {

    // private ScrollContainer sc;
    private Button btnTeste;
    private Container ctnTitleOSForm, ctnMain;
    private Label lbTitleOSForm, lbIdOS, lbTipoOS, lbLocalidadeOS, lbBairroOS, lbCidadeOS, lbListaUsuarios;
    private Edit editIdOS, editTipoOS, editLocalidadeOS, editBairroOS, editCidadeOS;
    FooterOrdemServicoList footerOrdemServicoForm;
    OrdemServico inputOrdemServico = new OrdemServico();
    private String msgTitleMsgGeneric, msgBodyMsgGeneric;
    private ComboBox comboBoxUsuarios;
    private List<User> usersList = new ArrayList<>();

    public OrdemServicoForm() {
        footerOrdemServicoForm = new FooterOrdemServicoList();
        ctnTitleOSForm = new Container();
        ctnMain = new Container();

        // get all users
        usersList = new UserDAO().getUsers();
        String usersName[] = new String[usersList.size()];
        if (usersList.size() > 0) {
            String users[][] = new String[usersList.size()][2];
            for (int i = 0; i < usersList.size(); i++) {
                User user = usersList.get(i);
                users[i] = new String[]{String.valueOf(user.getIdUser()), user.getNome()};
                usersName[i] = user.getNome();
                System.out.println("ID " + user.getIdUser() + "  NOME " + user.getNome());
            }
            ComboBox.usePopupMenu = false;
            comboBoxUsuarios = new ComboBox(usersName);
        }

        lbTitleOSForm = new Label("Nova ordem de serviço");

        lbIdOS = new Label("ID ");
        lbTipoOS = new Label("Tipo da Ordem de Serviço ");
        lbLocalidadeOS = new Label("Localidade ");
        lbBairroOS = new Label("Bairro ");
        lbCidadeOS = new Label("Cidade ");
        lbListaUsuarios = new Label("Selecione o usuário responsável por esta Ordem de Serviço ");

        editIdOS = new Edit();
        editTipoOS = new Edit();
        editLocalidadeOS = new Edit();
        editBairroOS = new Edit();
        editCidadeOS = new Edit();

        btnTeste = new Button("SALVA ORDEM DE SERVIÇO !");
    }

    @Override
    public void initUI() {
        int gap = fmH * 3;

        super.initUI();

        add(ctnTitleOSForm, LEFT, AFTER, FILL, gap);

        ctnTitleOSForm.add(lbTitleOSForm, LEFT + 50, AFTER, FILL - 50, PREFERRED);
        lbTitleOSForm.setBackForeColors(Colors.P_LIGHT, Color.WHITE);
        ctnTitleOSForm.setBackForeColors(Colors.P_LIGHT, Color.WHITE);

        add(ctnMain, LEFT, AFTER, FILL, FILL);
//        ctnMain.setBackForeColors(Color.GREEN, Color.WHITE);

        ctnMain.add(lbTipoOS, LEFT + 50, TOP + 100);
        ctnMain.add(editTipoOS, SAME, AFTER, FILL - 50, PREFERRED);

        ctnMain.add(lbLocalidadeOS, SAME, AFTER + 100);
        ctnMain.add(editLocalidadeOS, SAME, AFTER, FILL - 50, PREFERRED);

        ctnMain.add(lbBairroOS, SAME, AFTER + 100);
        ctnMain.add(editBairroOS, SAME, AFTER, FILL - 50, PREFERRED);

        ctnMain.add(lbCidadeOS, SAME, AFTER + 100);
        ctnMain.add(editCidadeOS, SAME, AFTER, FILL - 50, PREFERRED);

        ctnMain.add(lbListaUsuarios, SAME, AFTER + 100);
        ctnMain.add(comboBoxUsuarios, SAME, AFTER, FILL - 50, PREFERRED);

        ctnMain.add(btnTeste, SAME, AFTER + 100);

        footerOrdemServicoForm.setBackForeColors(Colors.P_LIGHT, Color.WHITE);
        add(footerOrdemServicoForm, LEFT, BOTTOM, FILL, gap);

        btnTeste.addPressListener(e -> {
            try {
                saveOS();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    public void saveOS() throws SQLException {
        inputOrdemServico.setTipoOS(editTipoOS.getText());
        inputOrdemServico.setLocalidadeOS(editLocalidadeOS.getText());
        inputOrdemServico.setBairroOS(editBairroOS.getText());
        inputOrdemServico.setCidadeOS(editCidadeOS.getText());

        int indexUsuario = comboBoxUsuarios.getSelectedIndex();
        if (indexUsuario > -1) {
            User user = usersList.get(indexUsuario);
            inputOrdemServico.setIdUser(user.getIdUser());
        }

        boolean returnFromValidator = OrdemServicoFormControl.ValidateOrdemServicoForm(inputOrdemServico);
        msgTitleMsgGeneric = "Cadastro de Ordem de serviço";
        msgBodyMsgGeneric = "Ordem de serviço cadastrada com sucesso";
        if (returnFromValidator == true) {
            MessageGeneric.successMessageBox(msgTitleMsgGeneric, msgBodyMsgGeneric);
            try {
                back();
            } catch (ElementNotFoundException ex) {
                Logger.getLogger(UserForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            msgBodyMsgGeneric = "Erro ao cadastrar ordem de serviço";
        }
    }

}
