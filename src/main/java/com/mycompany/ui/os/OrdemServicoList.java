/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ui.os;

import com.mycompany.ui.Default;
import com.mycompany.ui.usuarios.FooterUserForm;
import com.mycompany.util.Colors;
import totalcross.ui.Button;
import totalcross.ui.Container;
import totalcross.ui.Label;
import totalcross.ui.ScrollContainer;
import totalcross.ui.gfx.Color;

/**
 *
 * @author guilherme.ribas
 */
public class OrdemServicoList extends Default {

    private ScrollContainer sc;
    private Container ctnTitleOS;
    private Label lbTeste, lbTitleOS;
    private Button btnVoltar, btnNovaOS;
    FooterOrdemServicoList footerOrdemServicoList;
    private boolean firstBtn, secondBtn, thirdBtn;

    public OrdemServicoList() {
        footerOrdemServicoList = new FooterOrdemServicoList();
        ctnTitleOS = new Container();

        firstBtn = true;
        secondBtn = false;
        thirdBtn = true;
    }

    @Override
    public void initUI() {
        int gap = fmH * 3;

        super.initUI();

        add(ctnTitleOS, LEFT, AFTER, FILL, gap);

        lbTitleOS = new Label("Ordens de serviço cadastradas");
        ctnTitleOS.add(lbTitleOS, LEFT + 50, AFTER, FILL - 50, PREFERRED);
        lbTitleOS.setBackForeColors(Colors.P_LIGHT, Color.WHITE);
        ctnTitleOS.setBackForeColors(Colors.P_LIGHT, Color.WHITE);

        sc = new ScrollContainer(false, true);
        lbTeste = new Label("eae");
        add(sc, LEFT, TOP, FILL, FILL);
        sc.add(lbTeste, LEFT, CENTER);
        
        footerOrdemServicoList.setBackForeColors(Colors.P_LIGHT, Color.WHITE);
        add(footerOrdemServicoList, LEFT, BOTTOM, FILL, gap);
    }
    

}
