/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ui.os;

import com.mycompany.ui.Default;
import java.util.logging.Level;
import java.util.logging.Logger;
import totalcross.io.IOException;
import totalcross.ui.Button;
import static totalcross.ui.Container.BORDER_NONE;
import static totalcross.ui.Control.BOTTOM;
import static totalcross.ui.Control.CENTER;
import static totalcross.ui.Control.LEFT;
import totalcross.ui.image.Image;
import totalcross.ui.image.ImageException;
import totalcross.util.ElementNotFoundException;

/**
 *
 * @author guilherme.ribas
 */
public class FooterOrdemServicoList extends Default {

    public Button btnBack, btnNewOS;

    @Override
    public void initUI() {
        try {
            btnBack = new Button("", new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-back-50.png"), CENTER, 8);
            btnBack.setBorder(BORDER_NONE);
            add(btnBack, LEFT, BOTTOM);

            btnNewOS = new Button("", new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-plus-50.png"), CENTER, 8);
            btnNewOS.setBorder(BORDER_NONE);
            add(btnNewOS, RIGHT, SAME);
        } catch (ImageException | IOException ex) {
            ex.printStackTrace();
        }

        btnBack.addPressListener(e -> {
            System.out.println("clicou no VOLTAR ");
            try {
                back();
            } catch (ElementNotFoundException ex) {
                ex.printStackTrace();
            }
        }
        );

        btnNewOS.addPressListener(e -> {
            OrdemServicoForm ordemServicoForm = new OrdemServicoForm();
            ordemServicoForm.show();
        }
        );
    }

}
