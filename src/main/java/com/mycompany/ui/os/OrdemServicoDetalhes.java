/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ui.os;

import com.mycompany.ui.Default;
import com.mycompany.ui.usuarios.FooterUserForm;
import com.mycompany.util.Colors;
import totalcross.io.IOException;
import totalcross.ui.AlignedLabelsContainer;
import totalcross.ui.Button;
import totalcross.ui.Container;
import static totalcross.ui.Control.AFTER;
import static totalcross.ui.Control.FILL;
import static totalcross.ui.Control.LEFT;
import static totalcross.ui.Control.TOP;
import totalcross.ui.Edit;
import totalcross.ui.Label;
import totalcross.ui.TabbedContainer;
import totalcross.ui.event.DragEvent;
import totalcross.ui.event.PenEvent;
import totalcross.ui.event.PenListener;
import totalcross.ui.gfx.Color;
import totalcross.ui.image.Image;
import totalcross.ui.image.ImageException;

/**
 *
 * @author guilherme.ribas
 */
public class OrdemServicoDetalhes extends Default {

    /**
     * menuItem refere-se a aba maior do menu superior (a que contem a label)
     * subMenuItem refere-se a pequena barra abaixo o menuItem (identificador de
     * navegação)
     */
    private Container menuItensContainer, menuItem0, menuItem1, subMenuItem0, subMenuItem1, tcContainer0, tcContainer1, ctnInfos, ctnEventos, ctnForm;
    private final Label lbMenuItem1, lbMenuItem2, lbConteudoMenuItem2, lbEventosNaoObrigatorios, lbGrpRetornoCampo, lbRetornoCampo, lbObservacao;
    private final Label lbEventosObrigatorios, lbInicioDeslocamento, lbFimDeslocamento, lbInicioReparos;
    private final String[] whatFor = {"", "", ""};
    private final AlignedLabelsContainer alcInfos;
    private Button btnFimReparos, btnRetornoCampo, btnTeste;
    private Edit editGrpRetornoCampo, editRetornoCampo, editObservacao;
    private Image teste;
    FooterUserForm footerOrdemServicoDetalhes;
    String[] lbInfos = {"OS: ", "Tipo serviço: ", "Nome: ", "Telefone: ", "Localidade: ", "Bairro: "};
    String[] lbTextInfos = {"SDFDS", "INSPECAO", "Guilherme", "45999360217", "SAO LUIS", "CENTRO"};
    boolean firstBtn, secondBtn, thirdBtn;

    public OrdemServicoDetalhes() {
        footerOrdemServicoDetalhes = new FooterUserForm();
        alcInfos = new AlignedLabelsContainer();
        lbMenuItem1 = new Label("Detalhes");
        lbMenuItem2 = new Label("Material");
        lbGrpRetornoCampo = new Label("Grupo retorno de campo");
        lbRetornoCampo = new Label("Retorno de campo");
        lbObservacao = new Label("Observação");

        lbEventosObrigatorios = new Label("EVENTOS OBRIGATÓRIOS");
        lbInicioDeslocamento = new Label("Início Deslocamento: 10/12/19 16:37:34");
        lbFimDeslocamento = new Label("Fim Deslocamento: 10/12/19 16:38:35");
        lbInicioReparos = new Label("Início Reparos: 10/12/19 16:39:36");

        editGrpRetornoCampo = new Edit();
        editRetornoCampo = new Edit();
        editObservacao = new Edit();

        lbEventosNaoObrigatorios = new Label("EVENTOS NÃO OBRIGATÓRIOS");
        lbConteudoMenuItem2 = new Label("CONTEÚDO DA ABA DO MATERIAL 2");

        for (int i = 0; i < lbInfos.length; i++) {
            lbInfos[i] = lbInfos[i].concat(lbTextInfos[i]);
        }

        firstBtn = true;
        secondBtn = false;
        thirdBtn = true;

    }

    @Override
    public void initUI() {

        int gap = fmH * 3;
        int textBorder = 50;

        TabbedContainer tc = new TabbedContainer(whatFor);
        tc.transparentBackground = true;
        tcContainer1 = new Container();
        tcContainer1.setBackColor(Colors.BACKGROUND_GRAY_01);

        tcContainer0 = new Container();
        tcContainer0.setBackColor(Colors.BACKGROUND_GRAY_02);

        menuItensContainer = new Container();
        menuItensContainer.setBackColor(Color.RED);
        menuItensContainer.setBackColor(Colors.P_LIGHT);

        menuItem0 = new Container();
        menuItem0.setBackColor(Color.CYAN);
        menuItem0.setBackColor(Colors.P_LIGHT);

        menuItem1 = new Container();
        menuItem1.setBackColor(Color.CYAN);
        menuItem1.setBackColor(Colors.P_LIGHT);

        subMenuItem0 = new Container();
        subMenuItem1 = new Container();
        subMenuItem0.setBackColor(Colors.S_LIGHT);
        subMenuItem1.setBackColor(Colors.P_LIGHT);

        ctnInfos = new Container();
        // ctnInfos.setBackColor(Color.GREEN);

        ctnEventos = new Container();
        // ctnEventos.setBackColor(Color.RED);

        ctnForm = new Container();
        // ctnForm.setBackColor(Color.CYAN);

        btnFimReparos = new Button("Fim Reparos");
        btnFimReparos.setBackColor(Colors.GREEN_FOREST);

        try {
            btnRetornoCampo = new Button("", new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-search-24.png"), CENTER, 8);
            btnRetornoCampo.setBorder(BORDER_NONE);
            btnTeste = new Button("", new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-search-24.png"), CENTER, 8);
            btnTeste.setBorder(BORDER_NONE);
        } catch (ImageException | IOException ex) {
            ex.printStackTrace();
        }

        menuItem0.addPenListener(new PenListener() {
            public void penUp(PenEvent arg0) {
                // TODO Auto-generated method stub
                System.out.println("item1");
                tc.setActiveTab(0);
                subMenuItem0.setBackColor(Colors.S_LIGHT);
                subMenuItem1.setBackColor(Colors.P_LIGHT);
            }

            public void penDragStart(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDragEnd(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDrag(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDown(PenEvent arg0) {
                // TODO Auto-generated method stub
            }
        });

        menuItem1.addPenListener(new PenListener() {
            public void penUp(PenEvent arg0) {
                // TODO Auto-generated method stub
                System.out.println("item2");
                tc.setActiveTab(1);
                subMenuItem0.setBackColor(Colors.P_LIGHT);
                subMenuItem1.setBackColor(Colors.S_LIGHT);
            }

            public void penDragStart(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDragEnd(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDrag(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDown(PenEvent arg0) {
                // TODO Auto-generated method stub
            }
        });

        tc.setContainer(0, tcContainer1);
        tc.setContainer(1, tcContainer0);

        add(tc, LEFT + 50, BOTTOM, FILL - 50, SCREENSIZE + 95);
        add(menuItensContainer, LEFT, BEFORE + 150, FILL, SCREENSIZE + 10, tc);

        menuItensContainer.add(menuItem0, LEFT + 1, AFTER, SCREENSIZE + 50, SCREENSIZE + 10);

        menuItensContainer.add(menuItem1, AFTER, SAME, SCREENSIZE + 50, SCREENSIZE + 10);

        add(subMenuItem0, LEFT, AFTER, SCREENSIZE + 50, SCREENSIZE + 1);
        add(subMenuItem1, AFTER, SAME, SCREENSIZE + 50, SCREENSIZE + 1);
        menuItem0.add(lbMenuItem1, CENTER, CENTER);
        menuItem1.add(lbMenuItem2, CENTER, CENTER);

        // should be 25 / 50 / 25 = 100% // should ??
        // adiciona components (containers) ao container referente a aba 1
        tcContainer1.add(ctnInfos, LEFT + 50, TOP, FILL - 50, SCREENSIZE + 23);
        tcContainer1.add(ctnEventos, SAME, AFTER, FILL - 50, SCREENSIZE + 37);
        tcContainer1.add(ctnForm, SAME, AFTER, FILL - 50, SCREENSIZE + 25);

        // PRIMEIRO CONTAINER - COM TEXT DE INFOS
        alcInfos.setLabels(lbInfos, 0);
        ctnInfos.add(alcInfos, LEFT + textBorder, CENTER);

        // SEGUNDO CONTAINER - COM TEXT DE EVENTOS
        ctnEventos.add(btnTeste, LEFT + textBorder, TOP);
        ctnEventos.add(lbEventosObrigatorios, CENTER, AFTER, btnTeste);
        ctnEventos.add(lbInicioDeslocamento, CENTER, AFTER);
        ctnEventos.add(lbFimDeslocamento, RIGHT_OF, AFTER);
        ctnEventos.add(lbInicioReparos, RIGHT_OF, AFTER);
        ctnEventos.add(lbEventosNaoObrigatorios, CENTER, AFTER);
        ctnEventos.add(btnFimReparos, LEFT + textBorder, AFTER, FILL - 15, gap);

        // TERCEIRO CONTAINER - COM CAMPOS DE INPUT
        ctnForm.add(lbGrpRetornoCampo, LEFT + 50, TOP, FILL - 50, PREFERRED);
        ctnForm.add(editGrpRetornoCampo, SAME, AFTER);
        ctnForm.add(lbRetornoCampo, SAME, AFTER);
        ctnForm.add(editRetornoCampo, SAME, AFTER, FILL - 120, PREFERRED);
        ctnForm.add(btnRetornoCampo, AFTER, SAME, editRetornoCampo);
        ctnForm.add(lbObservacao, SAME, AFTER, editRetornoCampo);
        ctnForm.add(editObservacao, SAME, AFTER);

        // adiciona components (containers) ao container referente a aba 1
        tcContainer0.add(lbConteudoMenuItem2, CENTER, CENTER);

        footerOrdemServicoDetalhes.setBackForeColors(Colors.P_LIGHT, Color.WHITE);
        add(footerOrdemServicoDetalhes, LEFT, BOTTOM, FILL, gap);

        footerOrdemServicoDetalhes.showButtons(firstBtn, secondBtn, thirdBtn);

        lbMenuItem1.setForeColor(Color.WHITE);
        lbMenuItem2.setForeColor(Color.WHITE);

        lbMenuItem1.addPenListener(new PenListener() {
            public void penUp(PenEvent arg0) {
                // TODO Auto-generated method stub
                System.out.println("item1");
                tc.setActiveTab(0);
                subMenuItem0.setBackColor(Colors.S_LIGHT);
                subMenuItem1.setBackColor(Colors.P_LIGHT);
            }

            public void penDragStart(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDragEnd(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDrag(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDown(PenEvent arg0) {
                // TODO Auto-generated method stub
            }
        });

        lbMenuItem2.addPenListener(new PenListener() {
            public void penUp(PenEvent arg0) {
                // TODO Auto-generated method stub
                System.out.println("item2");
                tc.setActiveTab(1);
                subMenuItem0.setBackColor(Colors.P_LIGHT);
                subMenuItem1.setBackColor(Colors.S_LIGHT);
            }

            public void penDragStart(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDragEnd(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDrag(DragEvent arg0) {
                // TODO Auto-generated method stub
            }

            public void penDown(PenEvent arg0) {
                // TODO Auto-generated method stub
            }
        });
    }
}
