/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ui;

import totalcross.ui.Bar;
import totalcross.ui.Container;
import totalcross.ui.MainWindow;
import totalcross.ui.Control;
import totalcross.ui.ToolTip;
import totalcross.ui.Window;
import totalcross.util.ElementNotFoundException;
import totalcross.util.Vector;

/**
 *
 * @author guilherme.ribas
 */
public abstract class Default extends Container {

    public static Vector containerStack = new Vector(10);
    public boolean executarUpdateAoVoltarNaTela = false;
    protected Bar bar;

    @Override
    public void initUI() {
        super.initUI();
        MainWindow mw = MainWindow.getMainWindow();
        Control c = mw.getFirstChild();
        if (c instanceof ToolTip) {
            mw.remove(c);
        }
    }

//    public void showContainer(Object c) {
//        Default cd = (Default) c;
//        cd.show();
//    }
    public void show() {
        containerStack.push(this);
        ((MainWindow) MainWindow.zStack.items[0]).swap(this);
    }

    public void back() throws ElementNotFoundException {
        System.out.println("chamou o back " + this);
        this.back(true);
    }

    public void back(boolean value) throws ElementNotFoundException {
        try {
            if (MainWindow.zStack.size() == 1) {
                if (containerStack.size() >= 2) {
                    Object pop = containerStack.pop(); // pop ourself
                    pop = null;
                    Default d = (Default) containerStack.peek();
                    // d.executarUpdateAoVoltarNaTela = value;
                    Window.getTopMost().swap(d);
                    System.out.println("container Stack " + containerStack);
                }
            } else {
                Window w = (Window) MainWindow.zStack.peek();
            }
        } catch (ElementNotFoundException enfe) {
            System.out.println("container Stack " + containerStack);
            System.out.println("ENFE: " + enfe);
            MainWindow.minimize();
        }
    }

    protected void addButtomBar() {

    }

    protected void addButtonFooter() {

    }

    protected void addButtonFooterRight() {

    }

}
