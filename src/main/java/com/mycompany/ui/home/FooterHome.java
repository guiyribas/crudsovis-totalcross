/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ui.home;

import com.mycompany.dao.UserDAO;
import com.mycompany.ui.Default;
import com.mycompany.ui.os.OrdemServicoDetalhes;
import com.mycompany.ui.os.OrdemServicoList;
import com.mycompany.ui.usuarios.UserForm;
import java.util.logging.Level;
import java.util.logging.Logger;
import totalcross.io.IOException;
import totalcross.ui.Button;
import static totalcross.ui.Control.CENTER;
import totalcross.ui.Label;
import totalcross.ui.event.ControlEvent;
import totalcross.ui.event.PressListener;
import totalcross.ui.image.Image;
import totalcross.ui.image.ImageException;

/**
 *
 * @author guilherme.ribas
 */
public class FooterHome extends Default {

    private Button btnImgNewUser, btnOrdemServico;
    private Label lbSizeUsersList;

    @Override
    public void initUI() {
        int sizeUsersList = new UserDAO().usersListSize();
        add(new Label(String.valueOf(sizeUsersList) + " registros"), LEFT, AFTER, FILL, PREFERRED);
        try {
            btnImgNewUser = new Button("", new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-plus-50.png"), CENTER, 8);
            btnImgNewUser.setBorder(BORDER_NONE);
            add(btnImgNewUser, RIGHT, BOTTOM);
        } catch (ImageException | IOException ex) {
            Logger.getLogger(FooterHome.class.getName()).log(Level.SEVERE, null, ex);
        }

        btnOrdemServico = new Button("ORDENS DE SERVIÇO", BORDER_NONE);
        add(btnOrdemServico, CENTER, CENTER, PREFERRED, PREFERRED);

        btnImgNewUser.addPressListener(new PressListener() {
            // @Override
            public void controlPressed(ControlEvent e) {
                UserForm userForm = new UserForm();
                userForm.show();
            }
        });

        btnOrdemServico.addPressListener(e
                -> {
            System.out.println("ORDENS DE SERVIÇO");
            OrdemServicoList ordemServicoList = new OrdemServicoList();
            ordemServicoList.show();
        });
    }
}
