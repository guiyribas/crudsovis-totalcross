package com.mycompany.ui.home;

import com.mycompany.ui.usuarios.UsersList;
import com.mycompany.util.Colors;
import totalcross.ui.Label;
import totalcross.ui.MainWindow;
import totalcross.sys.Settings;
import totalcross.ui.gfx.Color;

public class Home extends MainWindow {

    UsersList usersList = new UsersList();
    FooterHome footer = new FooterHome();
    int gap = 50;

    public Home() {
        setUIStyle(Settings.MATERIAL_UI);
        Settings.uiAdjustmentsBasedOnFontHeight = true;
    }

    public void initUI() {
        super.initUI();
        footer.setBackColor(Colors.P_LIGHT);
        add(footer, LEFT, BOTTOM, FILL, gap);
        add(usersList, LEFT, TOP, FILL, FIT);
        usersList.show();
    }
}
