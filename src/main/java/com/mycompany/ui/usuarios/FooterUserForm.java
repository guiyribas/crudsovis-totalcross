/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ui.usuarios;

import com.mycompany.ui.Default;
import com.mycompany.ui.home.FooterHome;
import com.mycompany.ui.usuarios.UserForm;
import java.util.logging.Level;
import java.util.logging.Logger;
import totalcross.io.IOException;
import totalcross.ui.Button;
import totalcross.ui.Container;
import static totalcross.ui.Control.BOTTOM;
import static totalcross.ui.Control.CENTER;
import static totalcross.ui.Control.LEFT;
import static totalcross.ui.Control.RIGHT;
import totalcross.ui.Label;
import totalcross.ui.event.ControlEvent;
import totalcross.ui.event.PressListener;
import totalcross.ui.gfx.Color;
import totalcross.ui.image.Image;
import totalcross.ui.image.ImageException;
import totalcross.util.ElementNotFoundException;

/**
 *
 * @author guilherme.ribas
 */
/**
 * TODO: FooterUserForm é usando como footer da OS também
 *
 * @author guilherme.ribas
 */
public class FooterUserForm extends Default {

    public Button btnFirst, btnSecond, btnThird;

    @Override
    public void initUI() {
    }

    public void showButtons(boolean firstBtn, boolean secondBtn, boolean thirdBtn) {
        try {
            if (firstBtn) {
                btnFirst = new Button("", new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-back-50.png"), CENTER, 8);
                btnFirst.setBorder(BORDER_NONE);
                add(btnFirst, LEFT, BOTTOM);
            }

            if (secondBtn) {
                btnSecond = new Button("", new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-plus-minus-50.png"), CENTER, 8);
                btnSecond.setBorder(BORDER_NONE);
                add(btnSecond, CENTER, SAME);
                btnSecond.addPressListener(e -> {
                    System.out.println("clicou no SEGUNDO BOTAO ");
                    actionOnPressSecondButton();
                }
                );
            }

            if (thirdBtn) {
                btnThird = new Button("", new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-plus-50.png"), CENTER, 8);
                btnThird.setBorder(BORDER_NONE);
                add(btnThird, RIGHT, SAME);
                btnThird.addPressListener(e -> {
                    actionOnPressThirdButton();
                    System.out.println("clicou no TERCEIRO BOTAO ");
                }
                );
            }

        } catch (ImageException | IOException ex) {
            Logger.getLogger(FooterHome.class.getName()).log(Level.SEVERE, null, ex);
        }

        // primeiro botao sempre será o de voltar
        btnFirst.addPressListener(e -> {
            System.out.println("clicou no VOLTAR ");
            try {
                back();
            } catch (ElementNotFoundException ex) {
                ex.printStackTrace();
            }
        }
        );

    }

    public void actionOnPressSecondButton() {

    }

    public void actionOnPressThirdButton() {

    }
}
