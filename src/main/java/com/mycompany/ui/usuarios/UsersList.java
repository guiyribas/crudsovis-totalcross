package com.mycompany.ui.usuarios;

import com.mycompany.dao.UserDAO;
import com.mycompany.model.User;
import com.mycompany.ui.Default;
import com.mycompany.ui.home.Home;
import com.mycompany.util.MessageGeneric;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import totalcross.io.IOException;
import totalcross.res.Resources;
import totalcross.ui.Button;
import totalcross.ui.AccordionContainer;
import totalcross.ui.Label;
import totalcross.ui.MainWindow;
import totalcross.sys.Settings;
import totalcross.ui.Container;
import static totalcross.ui.Control.FILL;
import static totalcross.ui.Control.LEFT;
import static totalcross.ui.Control.RIGHT;
import static totalcross.ui.Control.TOP;
import totalcross.ui.Grid;
import totalcross.ui.ListContainer;
import totalcross.ui.MultiEdit;
import totalcross.ui.ScrollContainer;
import totalcross.ui.Window;
import totalcross.ui.dialog.MessageBox;
import totalcross.ui.event.ControlEvent;
import totalcross.ui.event.Event;
import totalcross.ui.gfx.Color;
import totalcross.ui.image.Image;
import totalcross.ui.image.ImageException;
import java.util.List;
import totalcross.ui.event.ListContainerEvent;

public class UsersList extends Default {

    private Button btnImgBack, btnImgNewUser;

    private ScrollContainer sc;
    private List<User> users = new ArrayList<>();
    private ListContainer lcUsers = new ListContainer();
    private Container ctnUser[];
    private int totalUsers, count = 0;
    private boolean updateUsersList = true;

    public static Image aplyColor(Image img, int color) {
        try {
            Image image = img.getCopy();
            image.applyColor2(color);
            return image;
        } catch (ImageException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void initUI() {
        super.initUI();
        sc = new ScrollContainer(false, true);
        add(sc, LEFT, TOP, FILL, FILL);

        lcUsers.getFlick().longestFlick = 15;
        lcUsers.setBackColor(Color.WHITE);
        sc.add(lcUsers, LEFT, TOP, FILL, FILL);
    }

    public void addUsersListElements() throws ImageException, IOException {

        // lcUsers.setBackColor(Color.BLUE);
        // sc.setBackColor(Color.YELLOW);
        // setBackColor(Color.RED);

        lcUsers.removeAllContainers();
        users = new UserDAO().getUsers();
        totalUsers = users.size();

        if (users.size() > 0) {
            ctnUser = new Container[users.size()];
            String userItems[][] = new String[users.size()][7];
            for (int i = 0; i < users.size(); i++) {
                User user = users.get(i);
                userItems[i] = new String[]{user.getNome(), user.getSobrenome(), user.getSexo(), user.getIdade(), user.getCpf(), String.valueOf(user.getEstadoCivil()), user.getObservacao()};

                ListContainer.Item listCtnItemUser
                        = new ListContainer.Item(getLayout(new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-user-64.png").hwScaledFixedAspectRatio((int) (48 * Settings.screenBPP), true)));
                listCtnItemUser.items = new String[]{"", "Nome ", user.getNome(), "", ""};
                listCtnItemUser.appObj = user;
                ctnUser[i] = listCtnItemUser;
                // ctnUser[i].setBackColor(Color.GREEN);
            }
            lcUsers.addContainers(ctnUser);
            lcUsers.highlightColor = lcUsers.getBackColor();
            lcUsers.autoScroll = true;
            lcUsers.requestFocus();
        }
    }

    @Override
    public void onEvent(Event event) {
        switch (event.type) {
            case ListContainerEvent.ITEM_SELECTED_EVENT:
                ListContainer.Item item = (ListContainer.Item) lcUsers.getSelectedItem();
                User user = (User) item.appObj;
                System.out.println(user.getIdUser() + user.getNome());
                UserForm userFormEditar = new UserForm(user.getIdUser());
                userFormEditar.show();
                // MainWindow.getMainWindow().swap(userFormEditar);
                lcUsers.setSelectedIndex(-1);
        }
    }

    @Override
    protected void addButtomBar() {
        bar.addButton(Resources.menu, true);
    }

    private ListContainer.Layout getLayout(Image leftImage) {
        ListContainer.Layout layout = lcUsers.getLayout(5, 2);
        try {
            layout.insets.set(100, 50, 50, 100);
            layout.leftImageEnlargeIfSmaller = false;
            layout.defaultLeftImage = leftImage;
            layout.defaultRightImage = aplyColor(new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-next-30.png"), 0xDCDCDC)
                    .hwScaledFixedAspectRatio((int) (2 * Settings.screenBPP), true);
            layout.controlGap = 30;
            layout.lineGap = 1;
            layout.boldItems[2] = false;
            layout.relativeFontSizes[1] = -5;
            layout.relativeFontSizes[2] = +2;
            layout.relativeFontSizes[3] = -15;
            layout.positions[3] = RIGHT;
            layout.setup();

        } catch (Exception ee) {
            MessageBox.showException(ee, true);
        }
        return layout;
    }

    @Override
    public void onSwapFinished() {
        super.onSwapFinished();
        try {
            addUsersListElements();
        } catch (ImageException | IOException ex) {
            ex.printStackTrace();
        }
    }

}
