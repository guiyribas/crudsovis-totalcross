/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ui.usuarios;

import com.mycompany.control.UserFormControl;
import com.mycompany.dao.UserDAO;
import com.mycompany.model.User;
import com.mycompany.ui.Default;
import com.mycompany.util.MessageGeneric;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import totalcross.io.IOException;
import totalcross.sys.Settings;
import totalcross.ui.Button;
import totalcross.ui.ComboBox;
import totalcross.ui.Container;
import static totalcross.ui.Control.AFTER;
import static totalcross.ui.Control.CENTER;
import static totalcross.ui.Control.FILL;
import static totalcross.ui.Control.LEFT;
import static totalcross.ui.Control.PREFERRED;
import static totalcross.ui.Control.RIGHT;
import static totalcross.ui.Control.TOP;
import totalcross.ui.Edit;
import totalcross.ui.Label;
import totalcross.ui.MultiEdit;
import totalcross.ui.Radio;
import totalcross.ui.RadioGroupController;
import totalcross.ui.ScrollContainer;
import totalcross.ui.gfx.Color;
import totalcross.ui.image.Image;
import totalcross.ui.image.ImageException;
import totalcross.util.ElementNotFoundException;

/**
 *
 * @author guilherme.ribas
 */
public class UserForm extends Default {

    private ScrollContainer sc, scRadio;
    private Edit editNome, editSobrenome;
    private Edit editIdade, editCpf;
    private Edit maskedEdit;
    private MultiEdit multiEditObservacao;
    private RadioGroupController sexoRadioGroup;
    private Radio masculino, feminino;
    private ComboBox comboBoxEstadoCivil;
    private Label lbEstadoCivil, lbSexo, lbTitleUserForm;
    private Button btnDelete, btnSaveUser;
    private boolean isEdit = false;
    protected int idUser;
    private String msgTitleMsgGeneric, msgBodyMsgGeneric;
    private Container ctnTitle;
    FooterUserForm footerUser;
    User inputUserForm = new User();
    String[] labels = {"Nome", "Sobrenome", "Sexo", "Idade", "CPF", "Estado Civil", "Observações"};

    public UserForm() {
        this(-1);
    }

    public UserForm(int id) {
        System.out.println("ID DO USUARIO NO CONSTRUTOR" + idUser);
        if (id == -1) {
            System.out.println("NOVO USUARIO");
            isEdit = false;
        } else {
            System.out.println("editar");
            isEdit = true;
            idUser = id;
        }
        footerUser = new FooterUserForm();
        msgTitleMsgGeneric = "Cadastro de usuário";
        msgBodyMsgGeneric = "Usuário cadastrado com sucesso";
        if (isEdit) {
            inputUserForm.setIdUser(idUser);
            msgBodyMsgGeneric = "Usuário editado com sucesso";
        }
    }

    @Override
    public void initUI() {
        int gap = fmH * 3;
        int halfGap = gap / 2;

        ctnTitle = new Container();
        add(ctnTitle, LEFT, AFTER, FILL, gap);

        try {
            btnSaveUser = new Button("", new Image("C:\\Users\\guilherme.ribas\\Documents\\NetBeansProjects\\CrudSovis\\src\\main\\java\\images\\icons8-save-50.png"), CENTER, 8);
            btnSaveUser.setBorder(BORDER_NONE);
        } catch (ImageException | IOException ex) {
            ex.printStackTrace();
        }

        lbTitleUserForm = new Label("Cadastrar novo usuário");
        ctnTitle.add(lbTitleUserForm, LEFT + 50, AFTER, FILL, PREFERRED);
        ctnTitle.add(btnSaveUser, RIGHT, TOP);

        footerUser.setBackForeColors(0xFFA500, 0xFFFFFF);
        lbTitleUserForm.setBackForeColors(0xFFA500, 0xFFFFFF);
        btnSaveUser.setBackForeColors(0xFFA500, 0xFFFFFF);
        ctnTitle.setBackForeColors(0xFFA500, 0xFFFFFF);

        // add(titleUser, LEFT, TOP, FILL, gap);
        // add(titleUser, LEFT, TOP, FILL, gap);
        add(footerUser, LEFT, BOTTOM, FILL, gap);
        boolean firstBtn = true;
        boolean secondBtn = false;
        boolean thirdBtn = false;

        footerUser.showButtons(firstBtn, secondBtn, thirdBtn);

        Edit.useNativeNumericPad = false;
        sc = new ScrollContainer(false, true);
        add(sc, LEFT, TOP + 300, FILL, FIT);

//        footerUser.setBackColor(Color.BLUE);
//        sc.setBackColor(Color.YELLOW);
//        titleUser.setBackColor(Color.RED);
        // NOME
        editNome = new Edit();
        editNome.caption = labels[0];

        // SOBRENOME
        editSobrenome = new Edit();
        editSobrenome.caption = labels[1];

        // IDADE
        editIdade = new Edit();
        editIdade.caption = labels[3];
        editIdade.setMode(Edit.CURRENCY);
        editIdade.setKeyboard(Edit.KBD_NUMERIC);

        // CPF
        editCpf = new Edit();
        editCpf.caption = labels[4];
        editCpf.setMode(Edit.CURRENCY);
        editCpf.setKeyboard(Edit.KBD_NUMERIC);

        // SEXO - radioGroup 
        lbSexo = new Label(labels[2], CENTER);
        sexoRadioGroup = new RadioGroupController();
        masculino = new Radio("Masculino", sexoRadioGroup);
        masculino.setChecked(true);
        feminino = new Radio("Feminino", sexoRadioGroup);

        // ESTADO CIVIL - comboBox
        String[] itemsEstadoCivil = {"Solteiro(a)", "Casado(a)", "Divorciado(a)", "Viúvo(a)"};
        lbEstadoCivil = new Label("Estado Civil", CENTER);
        ComboBox.usePopupMenu = false;
        comboBoxEstadoCivil = new ComboBox(itemsEstadoCivil);
        comboBoxEstadoCivil.caption = labels[5];

        // OBSERVAÇÃO
        multiEditObservacao = new MultiEdit();
        multiEditObservacao.caption = labels[6];

//        btnInsert = new Button("CADASTRAR USUÁRIO", (byte) 0);
//        btnInsert.setBackForeColors(0xFFA500, 0xFFFFFF);
        btnDelete = new Button("DELETAR USUÁRIO", (byte) 0);
        btnDelete.setBackForeColors(0xDFF2800, 0xFFFFFF);

        // edit?
        if (isEdit) {
            User userEdit = new UserDAO().getUserByID(idUser);

            userEdit.setIdUser(idUser);
            editNome.setText(userEdit.getNome());
            editSobrenome.setText(userEdit.getSobrenome());
            sexoRadioGroup.setSelectedItem(userEdit.getSexo());
            editIdade.setText(userEdit.getIdade());
            editCpf.setText(userEdit.getCpf());
            comboBoxEstadoCivil.setSelectedIndex(userEdit.getEstadoCivil());
            multiEditObservacao.setText(userEdit.getObservacao());

            lbTitleUserForm.setText("Editar usuário");
        }

        sc.add(editNome, LEFT + 50, AFTER + 100, FILL - 50, PREFERRED + gap);
        sc.add(editSobrenome, LEFT + 50, AFTER + 50, FILL - 50, PREFERRED);
        sc.add(editIdade, LEFT + 50, AFTER + 50, FILL - 50, PREFERRED);
        sc.add(editCpf, LEFT + 50, AFTER + 50, FILL - 50, PREFERRED);
        sc.add(lbSexo, LEFT + 50, AFTER + 50);
        sc.add(lbEstadoCivil, AFTER + 500, SAME);
        sc.add(masculino, LEFT + 50, AFTER + 50);
        sc.add(feminino, LEFT + 50, AFTER + 50);
        sc.add(comboBoxEstadoCivil, AFTER + 50, SAME, lbEstadoCivil);
        sc.add(multiEditObservacao, LEFT + 50, AFTER + 50, FILL - 50, 400, feminino);
        // sc.add(btnInsert, LEFT + 50, AFTER + 50, SAME, PREFERRED);
        if (isEdit) {
            sc.add(btnDelete, LEFT + 50, AFTER + 50, SAME, PREFERRED);
        }

        btnSaveUser.addPressListener(e
                -> {
            saveUser();
        }
        );

//        btnInsert.addPressListener(e
//                -> {
//            saveUser();
//        }
//        );
        btnDelete.addPressListener(e
                -> {
            msgTitleMsgGeneric = "Exclusão de usuário";
            msgBodyMsgGeneric = "Usuário excluído com sucesso";
            try {
                new UserDAO().deleteUser(idUser);
                MessageGeneric.successMessageBox(msgTitleMsgGeneric, msgBodyMsgGeneric);
                back();
            } catch (SQLException | ElementNotFoundException ex) {
            }
        }
        );
    }

    public void saveUser() {

        inputUserForm.setNome(editNome.getText());
        inputUserForm.setSobrenome(editSobrenome.getText());
        inputUserForm.setSexo(sexoRadioGroup.getSelectedItem().getText());
        inputUserForm.setIdade(editIdade.getText());
        inputUserForm.setCpf(editCpf.getText());
        inputUserForm.setEstadoCivil(comboBoxEstadoCivil.getSelectedIndex());
        inputUserForm.setObservacao(multiEditObservacao.getText());
        boolean returnFromValidator = UserFormControl.ValidateUserForm(labels, inputUserForm, isEdit);
        if (returnFromValidator == true) {
            MessageGeneric.successMessageBox(msgTitleMsgGeneric, msgBodyMsgGeneric);
            try {
                back();
            } catch (ElementNotFoundException ex) {
                Logger.getLogger(UserForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            msgBodyMsgGeneric = "Erro ao cadastrar usuário";
        }
    }

    @Override
    public void onSwapFinished() {
        super.onSwapFinished();
    }
}
